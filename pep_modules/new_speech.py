import qi
import argparse
import sys
import time
import functools
from six.moves import queue
import struct
import numpy as np
import json
import time
import requests
import threading


import subprocess
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import io
import os
import argparse

from beamformer import RT_BF

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = "pep_modules/google_project.json"

class listener(object):

    def __init__(self, session, callback, Min_buff_len = 30, send_to_footer = True,
             Noise_Threshold = 0.3, Averaging_time_period = 1.5, Abortion_Time_Period = 2, max_input_len = 10):

        #inhereteence
        self.session = session
        super(listener, self).__init__()

        # qi services
        self.audio_service = session.service("ALAudioDevice")
        self.tablet_service = session.service('ALTabletService')
        self.AL = self.session.service("ALAutonomousLife")

        #init pepper touch responce
        self.memory = self.session.service("ALMemory")
        super(listener, self).__init__()
        self.touch = self.memory.subscriber("TouchChanged")
        self.touchId = self.touch.signal.connect(functools.partial(self.on_touched, "TouchChanged"))

        #attributes
        self._buff = queue.Queue()
        self.requestThread = None
        self.subscriber = callback
        self.tot_samples_proccessed = 0

        #flags
        self.stage = None
        self.aborted = False

        #values for stage finding thresholds
        self.levels_buffer = []
        samplerate = 16000
        self.noise_threshold = Noise_Threshold

        #the time over which an average of noise levels will be taken
        self.averaging_time_period = Averaging_time_period * samplerate 
        self.abortion_time_period = Abortion_Time_Period * samplerate

        #the period of time in samples by which the diologue will force the end of user input
        self.max_time = max_input_len * samplerate

        #a buffer length. This will let the stream include audio to a certain time before the touch was regestered, to help with latency issues.
        self.min_buff_len = Min_buff_len

        #A module to perform simple forward facing beamforming on the signal.
        offset = 4
        self.beamformer = RT_BF(offset) 

        #regester self as service on the qi
        self.module_name = "listener"
        session.registerService(self.module_name, self)

        return

    def start(self):

        # ask for the front microphone signal sampled at 16kHz
        self.audio_service.setClientPreferences(self.module_name, 16000, 0, 0) # was (16000, 3, 0) 
        self.audio_service.subscribe(self.module_name)
        self.stage = 'inactive'


    #-------------------------------------------------------------

    #                          Activation

    #-------------------------------------------------------------
    def start_listening(self):
        #a function to manualy start streaming audio to the given api
        self.on_activation()


    def on_touched(self, strVarName, value):
        """ 
        This will be called by qi each time a touch is detected.
        """
        for p in value:
            if p[1] and p[0] == 'Head':
                self.on_activation()


    def on_activation(self):

        '''
        controlles the startup
        '''

        if self.stage != 'inactive':
            ''' this should be considered an attempt to restart '''
            print 'second touch: restarting aortion period and timeout'

            self.send_to_footer('Second touch attempted')

            #restarting timers, there is a possibility that google will timeout the audio if anything goes wrong.
            self.tot_samples_proccessed = 0

            return

        #reseting values
        self.aborted = False

        print('here')

        #stop pepper from moving its loud limbs
        self.AL.setAutonomousAbilityEnabled("All", False)

        #beggin listening
        self.start_streaming()

        #mark flag as speaking
        self.stage = 'speaking'



    #-------------------------------------------------------------

    #                   Intercepting the stream

    #-------------------------------------------------------------

    def processRemote(self, nbOfChannels, nbOfSamplesByChannel, timeStamp, inputBuffer):

        #convert the 4 channel interleaved signal to float32
        signal = np.asarray(struct.unpack('<{}h'.format(nbOfSamplesByChannel * nbOfChannels), inputBuffer)).astype(np.float32) / 3276.7
        #combine to 1 channel after beamforming to concentrate signal on the front
        try:
            signal = self.beamformer.process( signal, 16000 )
        except Exception as e:
            print( e )
        #add gain
        signal = self.add_gain( 2.0, signal )

        #find stage
        if self.stage == 'speaking':

            self.find_stage( signal )
        
        try:

            #convert back to byte string
            signal = struct.pack( '<{}h'.format( len( signal )), * ( signal * 3276.7 ))
        
        except Exception as e:
            print( e )

        #keep buffer size to minimum
        while self._buff.qsize() > self.min_buff_len:
            chunk = self._buff.get()

        #add input buffer to our self managed buffer
        self._buff.put( signal )

        return


    #-------------------------------------------------------------

    #                     stream manipulation

    #-------------------------------------------------------------

    def add_gain( self, gain, signal ):

        for i in range( len( signal )):
            signal[i] = min(signal[i] * gain, 1.0)
        return signal


    #-------------------------------------------------------------

    #                     Sending a request

    #-------------------------------------------------------------

    def start_streaming(self):

        self.send_to_footer('I am listening')

        #send a wit request in a separate thread
        if self.requestThread != None:
            print('waiting for request thread')
            self.requestThread.join()

        print('starting streaming')
        
        self.requestThread = threading.Thread(target=self.stream)
        self.requestThread.start()


    def stream(self):

        client = speech.SpeechClient()

        #headers for request
        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=16000,
            language_code='en-US')
        streaming_config = types.StreamingRecognitionConfig(config=config)


        # streaming_recognize returns a generator.
        requests = (types.StreamingRecognizeRequest(audio_content=chunk)
            for chunk in self.stream_generator())

        try:
        # streaming_recognize returns a generator.
            responses = client.streaming_recognize(streaming_config, requests)
        except Exception as e:
            print ( e )

        has_given_result = False

        for response in responses:
            # Once the transcription has settled, the first result will contain the
            # is_final result. The other results will be for subsequent portions of
            # the audio.

            for result in response.results:
                print('Finished: {}'.format(result.is_final))
                print('Stability: {}'.format(result.stability))
                alternatives = result.alternatives
                # The alternatives are ordered from most likely to least.

                for alternative in alternatives:
                    print('Confidence: {}'.format(alternative.confidence))
                    print(u'Transcript: {}'.format(alternative.transcript))
                    self.responce_handler(alternative.transcript)

                has_given_result = True

        #check here to see if there has been a result, else 
        if not has_given_result:
            self.send_to_footer('Im sorry, can you try saying that again')

    def stream_generator(self):

        print ('generator called')

        while self._buff.qsize() > self.min_buff_len:
            chunk = self._buff.get()

        while True:
            #take next buffer
            chunk = self._buff.get()

            #none indicates the end of a stream
            if chunk is None: 
                print('found a none')
                return 

            yield str(chunk)

    def end_stream(self):
        print('putting None in buffer')
        #end the stream
        self._buff.put(None)

        #reset atributes for the next speech session
        self.tot_samples_proccessed = 0
        self.stage = 'inactive'
        self.AL.setAutonomousAbilityEnabled("All", True)


    def responce_handler(self, responce):

        self.send_to_footer("I heard '{}'".format(responce))

        #we should perhaps send more info to the subscriber
        self.subscriber(responce)

        return

    #-------------------------------------------------------------

    #                    assessing the stages

    #-------------------------------------------------------------

    def find_stage(self, signal):

        level = self.calcLevel(signal)  
        print('level: {}'.format(level))    

        if self.stage == 'speaking':

            #increment total samples
            self.tot_samples_proccessed += len(signal)

            #check for timeout
            if self.tot_samples_proccessed > self.max_time:
                self.send_to_footer('speak time out: max length is {} seconds.'.format(self.max_time / samplerate))
                print('max time exceeded -- ending stream')
                self.end_stream()

            #if there is no speech happening
            if level <= self.noise_threshold and self.tot_samples_proccessed > self.averaging_time_period:

                print(self.tot_samples_proccessed, self.abortion_time_period)
                
                #abort if under a set time period
                if (self.tot_samples_proccessed <= self.abortion_time_period):
                    self.aborted = True

                #otherwise it is finished
                else:
                    self.send_to_footer('speech finished, thinking ...')
                
                self.end_stream()
        return

    def calcLevel(self, data):
        #Calculate noise level

        #push new level on to end of array 
        self.levels_buffer = np.append(self.levels_buffer, data)

        #and dispose of the oldest level if buffer s of length
        if len(self.levels_buffer) > self.averaging_time_period:
            self.levels_buffer = self.levels_buffer[len(data):]

        avv_levels = np.sum(map(abs, self.levels_buffer)) / len(self.levels_buffer)

        return avv_levels


    def send_to_footer(self, message):
        '''execute JS for footer'''

        print('sending to footer: {}'.format(message))

        self.tablet_service.executeJS('show_in_footer("' + message + '");')
        return
