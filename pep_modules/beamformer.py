
#----------------------------------------------------------------------------------------------------------------------------------
#
# This is a class to perform a simple beamforming routine on incoming signal.
# the two front mics are delayed by a given sumber of samples (idealy 3 or 4)
# this time represents the time it takes for sound to travel from the front to the back mics
# this action re-alligns and boosts any signal ariving from the front while forcing signals from un-desired directions
# out of sinc. thus boosting the amplitute of the desired signal.
# this class could be improved by identifying the direction a desired signal is coming from and agusting the delays accordingly.
#
#-----------------------------------------------------------------------------------------------------------------------------------
import numpy as np

class RT_BF:
    
    def __init__(self, offset):
        self.offset_num = offset
        self.lastOffsetBuffer = np.zeros(offset)
        return
    
    def process(self, signal, samplerate):

        #separate the interleaved file
        first = np.zeros(len(signal) / 4)
        second = np.zeros(len(signal) / 4)
        third = np.zeros(len(signal) / 4)
        forth = np.zeros(len(signal) / 4)

        ind  = 0
        for i in range(0, len(signal), 4):
            first[ind] = signal[i] 
            second[ind] = signal[i+1]
            third[ind] = signal[i + 2]
            forth[ind] = signal[i + 3]
            ind += 1

        #rename them for corresponding mics
        fl = forth 
        fr = third 
        bl = second
        br = first

        #save the leftovers for the next process call
        self.lastOffsetBuffer = (fl + fr)[0-self.offset_num:]
        
        #beam-form and re-merge the signal into on channel
        front = np.append( self.lastOffsetBuffer, (fl + fr)[:0-self.offset_num] )

        beamed = (front + bl + br) / 4
        
        return beamed
    
    def reset(self):
        self.lastOutputBuffer = np.zeros(self.offset_num)




#---------------------------------------------------------------------------------------------
#
#              An Infinite impulse responce filter that can help clean the mic noise
#
#---------------------------------------------------------------------------------------------
