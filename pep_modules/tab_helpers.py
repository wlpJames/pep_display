import qi
import sys
import json


class tablet_coms_service:

  def __init__(self, session):
    #define a signal 'tabletInput'
    self.tablet_to_pepper = qi.Signal()
    self.pepper_to_tablet = qi.Signal()

    self.s = session

    #let's register our service with the name "tablet_coms"
    self.id = self.s.registerService("tablet_coms", self)

  def disconnect(self):
    print 'disconecting all'
    #self.tablet_to_pepper.disconnectAll()
    #self.pepper_to_tablet.disconnectAll()

    #unregester the service

    self.s.unregisterService(self.id)


  #define a bang method that will trigger the onBang signal
  def send_request_to_pepper(self, data):
    #trigger the signal with 42 as value
    self.tablet_to_pepper(data)

  def send_to_tablet(self, data):
    #comment...
    self.pepper_to_tablet(data)

