/*/////////////////////////////////////////////////////////////////////////////////

	Here:
		Errors are caught and sent to the app in python
		A function pep_log is defined to be used to log to the python terminal
		Console.log() is overriden and now points towards 

*//////////////////////////////////////////////////////////////////////////////////)

//a function to pipe console messages to pepper
window.onerror = function(msg, url, line, col, error) {
	error = {message : msg, url : url, line : line, col : col, error : error}
	pepper_request('ERROR', 'CONSOLE', error);
}

//a function to pipe logs to pepper 
function pep_log(message) {
	//this should be able to deal with vars other than strings?
	pepper_request('LOG', 'CONSOLE', message);
}

console = {}
console.log = function(message){
	pep_log(message);
}

window.console = console


/*////////////////////////////////////////////////////////////

				This code will define the requests

*/////////////////////////////////////////////////////////////

//dict holding all the js callbacks
var callbacks = [];
var callback_id_counter = 0;

//a function to send incoming data to the correct callback
function callback_director(data)
{
	data = JSON.parse(data)
	for (var i = 0; i < callbacks.length; i++) {

		//alert(callbacks[i]['callback_id']);

		if (callbacks[i]['callback_id'] == data['callback_id']) {

			//carry out function
			callbacks[i]['callback'](data['data']);

			//as callbacks here are single use, pop this from the list
			callbacks.splice(i, 1);

			return
		}

	}

	//so no matching callback
	alert('there has been an error in the callbacks')

}

//a script to regester the callback direction function
QiSession(function (session) {


		session.service("tablet_coms").then(
			function (coms) {
				coms.pepper_to_tablet.connect(callback_director);
			}, function (error) {
				alert("An error occurred: " + error);
			}
		);

	
	}, function () {
		alert("disconnected");
	}
);

//a function to define and send messages to the controler
function pepper_request(request_type, route, data, callback) {

	if (callback == NaN) {
		callback = null;
	}

	if (data == NaN) {
		data = '';
	}
	//alert('here')
	//connect to a sesison
	QiSession(function (session) {
		//sessions cal only be accessed within this callback...
			session.service("tablet_coms").then(
				
				function (coms) {

					if (callback != null) {
						//add callback to a list of callbacks
						//increment callback id
						callbacks.push({callback : callback, callback_id : callback_id_counter})

					}

					//write message
					message = {
						request_type : request_type,
						route : route,
						data : data,
						callback_id : callback_id_counter
					};

					//send message
					coms.send_request_to_pepper(JSON.stringify(message));

				}, function (error) {

					alert("An error occurred: " + error);
				}
			);
		
		}, function () {
			alert("disconnected");
		}
	);
}





//=======================================================================
//
//=======================================================================
//
//
//							THE FOOTER!!!!
//
//
//=======================================================================
//
//=======================================================================



//=======================================================================        

//				Functions for interacting with footer

//=======================================================================


function show_in_footer(text) {
	document.getElementById('pep_says').innerHTML = text;
	document.getElementById('footer').style.display = 'block';
	document.getElementById('footer').style.height = '30%'
    document.getElementById('footer').style.opacity = 1;
    setTimeout(dismiss_footer, 3000)

}

function dismiss_footer() {
	document.getElementById('footer').style.display = 'hidden';
	document.getElementById('footer').style.height = '0%'
    document.getElementById('footer').style.opacity = 0;
}


//when the rest of the document is ready
document.addEventListener('DOMContentLoaded', function(event) {


	//=======================================================================        

	//				Appending the footer to the end of the body

	//=======================================================================

	footer = "<footer id='footer' class='footer'> \
	    		<button id='dismiss' onclick='dismiss_footer()'>X</button> \
	    		<h3 id='pep_says'></h3> \
	    	</footer>";
	document.body.innerHTML += footer
	dismiss_footer()

})



