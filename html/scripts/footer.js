
//=======================================================================        

//				Functions for interacting with footer

//=======================================================================


function show_in_footer(text) {
	document.getElementById('pep_says').innerHTML = text;
	document.getElementById('footer').style.display = 'block';
}

function dismiss_footer() {
	document.getElementById('footer').style.display = 'none';
}


//=======================================================================        

//				Appending the footer to the end of the body

//=======================================================================

footer = "<footer id='footer' class='footer'> \
    		<button id='dismiss' onclick='dismiss_footer()'>X</button> \
    		<h3 id='pep_says'></h3> \
    	</footer>";
document.body.innerHTML += footer




//=======================================================================        

//				Adding css for the footer from footer.css

//=======================================================================

// Get HTML head element 
var head = document.getElementsByTagName('HEAD')[0];  

// Create new link Element 
var link = document.createElement('link'); 

// set the attributes for link element  
link.rel = 'stylesheet';  

link.type = 'text/css'; 

link.href = 'style.css';  

// Append link element to HTML head 
head.appendChild(link);