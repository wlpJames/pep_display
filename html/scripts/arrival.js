console.log('loading file')

function showDropdown() {
  document.getElementById("myDropdown").classList.toggle("show");
}

function focusHost() {
    document.getElementById("name").style.display = "none";
    document.getElementById("container").style.paddingTop = "5%";
    document.getElementById("submit").style.display = "none"
    document.getElementsByClassName("heading")[0].style.display = "none"
    showDropdown()

}
function unFocusHost() {
    setTimeout(function() { 
        document.getElementById("name").style.display = "block";
        document.getElementById("submit").style.display = "block"
        document.getElementById("container").style.paddingTop = "10%";
        document.getElementsByClassName("heading")[0].style.display = "block"
        showDropdown()
    }, 30);
}

function focusGuest() {
    document.getElementById("host-name").style.display = "none";
    document.getElementById("container").style.paddingTop = "5%";
    document.getElementById("submit").style.display = "none"
}
function unFocusGuest() {
    document.getElementById("host-name").style.display = "block";
    document.getElementById("submit").style.display = "block"
    document.getElementById("container").style.paddingTop = "10%";
}


//called on input to search a db for a host name
function find_host_suggestion() 
{
    
    //a function that sends a pepper request to the app with a request to search a db for suggestions
    request_type = 'GET';
    route = 'arrival/JSON';
    data = {
        search: document.getElementById('myInput').value
    };

    pepper_request(request_type, route, data, function(data) {

        //creat html string of the contents up to max len
        list = '';
        var max_len = 3;
        for (var i = 0; i < Math.min(data.length, max_len); i++) {
            list += '<p onclick="trans_val(this)">' + data[i]['name'] + '</p>'
        }

        document.getElementById('myDropdown').innerHTML = list;

    });
    
}

function submit_form()
{
    request_type = 'POST';
    route = 'arrival'
    data = {
        name : document.getElementById('name').value,
        host : document.getElementById('host-name').value
    }
    pepper_request(request_type, route, data)
}

function trans_val(ell) 
{
    document.getElementById('myInput').value = ell.innerHTML;
}