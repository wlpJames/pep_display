//alert('BANG')
function callback_func(data) {
	document.getElementById('display_area').innerHTML = data
}

function send_data_request() {
	request_type = 'GET';
	route = 'index/JSON';
	data = {
		a: 1.25,
		b: 2,
		c: "This is a message"
	};
	pepper_request(request_type, route, data, callback_func);
}

function new_page() {
	request_type = 'GET'
	route = 'content'
	data = ''
	pepper_request(request_type, route, data)
}

function go_to_form_page() 
{
	request_type = 'GET'
	route = 'arrival'
	data = ''
	pepper_request(request_type, route, data)
}

function go_to_questionair() {

	request_type = 'GET'
	route = 'questionair'
	data = ''
	pepper_request(request_type, route, data)

}

//this is a funtion that shows of the capability to log errors and console prints
function log_show()
{
    console.log('the tablet redirects console prints; it will also do errors')

    //call a fake function to get an error
    this_is_an_example_error('it will show in your terminal')
}