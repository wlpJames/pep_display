function build_form() {	
    //a function that sends a pepper request to the app with a request to search a db for suggestions
    request_type = 'GET';
    route = 'questionair/JSON';
    data = ''

    pepper_request(request_type, route, data, function(questions) {

		//places questions into a form-like table
		str = '<table>';
		for (i = 0; i < questions.length; i++) {
			str += '<tr id="row_' + String(i) + '" class = "QandA"><td class= "q">' + questions[i]['question'] + '</td><td><input id ="'+ String(i) + '" type="text" class="ans" onfocusin="focus_on_question(this)" onfocusout="unfocus_question(this)"></td></tr>';
		}
		str += '</table>'

		//add a larger free entry feild on the end
		str += '<h4>Comments:</h4><textarea id= "'+ String(questions.length) + '"rows="4" class="free-entry" onfocusin="focus_on_question(this)" onfocusout="unfocus_question(this)"></textarea><button class="sub" onclick="formComplete()">Submit</button>'

		document.getElementById('questionair').innerHTML = str;
	
    });
}
//document.addEventListener('readystatechange', build_form);

//hide all other questions that are not this question
function focus_on_question(question) {	
	var classname = document.getElementsByClassName("QandA");
	for (var i = 0; i < classname.length; i++) {
		if (question.parentNode.parentNode == classname[i]) {
		}
		else {
			classname[i].style.display = "none";
		}
	}
};

//hide single question and transfer contents to the right place
function unfocus_question(question) {

	var classname = document.getElementsByClassName("QandA");

	for (var i = 0; i < classname.length; i++) {

		if (question.parentNode.parentNode == classname[i]) {	
		}
		else {
			classname[i].style.display = "block";
		}
	}
} 

function formComplete() {
	
	//gather data
	var rows = document.getElementsByClassName('QandA')
	data = []
	for (var i = 0; i < rows.length; i++) {
		ques = rows[i].childNodes[0].innerHTML
		ans =  rows[i].childNodes[1].childNodes[0].value
		data.push({question : ques, answer : ans})
	}

	//send data
	request_type = 'POST';
    route = 'questionair'
    pepper_request(request_type, route, data)

    //redirect to index page
    pepper_request('GET', 'index', '')

}

function back() {
	request_type = 'GET'
	route = 'index'
	data = ''
	pepper_request(request_type, route, data)
}