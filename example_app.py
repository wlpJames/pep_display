
import argparse
import qi
import sys
import dialogflow_v2 as dialogflow

from pep_modules.new_speech import listener
from pep_modules.app_framework import app_framework

class test_app:
	'''
		This is a very simple app to show how the app_framework could be 
		used in a method that emulates the flask server framework

		The app_framework can also be used more simply as a messenger between the tablet and the pepper
		in this case it will still be necessary to to disting

	'''

	def __init__(self, app):

		#set a framework that can organise comunication --- the app route here is a function that will recieve data sent by the 
		#tablet, this will be in a form specified in the docs
		self.framework = app_framework(app.session, self.app_route)
		self.framework.set_DB('template_app.db')
		#show index/home page 
		self.framework.display_page('index.html')
		self.app = app
		self.session = app.session

		#In this example a speach service in initiated that shows how simple vocal interactions can go down
		api = "ALAnimatedSpeech"
		self.AS = self.session.service(api)

		#A module that will listen for vocal commands from users
		self.speach_routine = listener(self.app.session, self.speech_callback) 
		self.speach_routine.start()



	def app_route(self, request):
		'''
			This is a weights function that takes a request from the tablet and will decide which page/function it refers to and what is asking to be done

			be creative here!! im basing mine of a flask server app.py file, but do what you like

			see the docs for the request formating
		'''
		#-----------------------------------------------------------------------------------------------
		#
		#										   INDEX PAGE
		#
		#-----------------------------------------------------------------------------------------------
		if request['route'] == 'index':
			
			if request['request_type'] == 'POST':
				#send new page or summin
				return

			elif request['request_type'] == 'GET':
				self.framework.display_page("index.html")


		#an example of a call for data
		if request['route'] == 'index/JSON':

			if request['request_type'] == 'GET':

				#This is just to show that console messages are redirected
				#this is also the prefered methed to acces ALTabletService as two instances might cause trouble
				return_data = {
								'data': 'message recieved',
								'callback_id' : request['callback_id']
							  }

				self.framework.send_to_tablet(return_data)



		#-----------------------------------------------------------------------------------------------
		#
		#										   CONTENT PAGE
		#
		#-----------------------------------------------------------------------------------------------
		
		if request['route'] == 'content':

			if request['request_type'] == 'GET': 
				self.framework.display_page("content.html")
				self.AS.say('this is an example page')



		#-----------------------------------------------------------------------------------------------
		#
		#										   ARIVALS PAGE
		#
		#-----------------------------------------------------------------------------------------------

		#an example of a second page
		if request['route'] == 'arrival':

			if request['request_type'] == 'POST':
				#add post information into DB and return a new page
				#TODO
				self.framework.display_page('arrival_confirmed.html')
				return

			elif request['request_type'] == 'GET':
				self.framework.display_page("arrival.html")
				return

		if request['route'] == 'arrival/JSON':
		
			#but for now we will hard code it
			rows = []
			rows += [ {'name' : row[0], 'department' : row[1]}
						for row in self.framework.DB_execute('SELECT * FROM hosts')]

			return_data = { 
							'data' : rows,
							'callback_id' : request['callback_id']
						  }

			self.framework.send_to_tablet(return_data)

		#-----------------------------------------------------------------------------------------------
		#
		#										QUESTIONAIR PAGE
		#
		#-----------------------------------------------------------------------------------------------

		if request['route'] == 'questionair':

			if request['request_type'] == 'GET':

				self.framework.display_page('questionair.html')

			if request['request_type'] == 'POST':
				form = request['data']
				print(form)

		if request['route'] == 'questionair/JSON':

			#quirey DB 
			r = self.framework.DB_execute('SELECT * FROM questions')

			#reformat the results
			rows = []
			rows += [{'question' : row[0]} for row in r ]

			#return data 
			return_data = {
				'data' : rows,
				'callback_id' : request['callback_id']
			}
			self.framework.send_to_tablet(return_data)

		if request['route'] == 'say_hello':
			self.AS.say('hello')
			self.framework.display_page('index.html')

		return

	def speech_callback(self, message):
		'''
			Any speech that is recognised will be passed directly to this function as a string
		'''

		#send this to a diologue flow agent and recieve a responce
		responce = send_to_diologueFlow(message)
		print responce

		AS.say(responce)

		return

def send_to_diologueFlow(inpt):

	project_id = 'spatial-edition-240815'
	session_id = '1'
	texts = ['hello how are you', 'what are you doing', 'where is the toilet', 'what can you do?', 'what is the capital of russia?']
	lan = 'en-US'
    
    #setup session
    session_client = dialogflow.SessionsClient()
    session = session_client.session_path(project_id, session_id)
    print('Session path: {}\n'.format(session))

    #params
    text_input = dialogflow.types.TextInput(
        text=inpt, language_code=language_code)
    query_input = dialogflow.types.QueryInput(text=text_input)

    #the request
    response = session_client.detect_intent(
        session=session, query_input=query_input)

    #diognostics
    print('=' * 20)
    print('Query text: {}'.format(response.query_result.query_text))
    print('Detected intent: {} (confidence: {})\n'.format(
        response.query_result.intent.display_name,
        response.query_result.intent_detection_confidence))
    print('Fulfillment text: {}\n'.format(
        response.query_result.fulfillment_text))

    return responce.query_result.fulfillment_text


def main():
	#set up pepper session
	parser = argparse.ArgumentParser()
	parser.add_argument("--ip", type=str, default="198.18.0.1",
	                    help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
	parser.add_argument("--port", type=int, default=9559,
	                    help="Naoqi port number")

	args = parser.parse_args()

	try:
	    # Initialize qi framework.
	    connection_url = "tcp://" + args.ip + ":" + str(args.port)
	    app = qi.Application(["Stream_to_wit", "--qi-url=" + connection_url])
	except RuntimeError:
	    print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
	           "Please check your script arguments. Run with -h option for help.")
	    sys.exit(1)

	app.session.connect('192.168.0.116')
	session = app.session
	main = test_app(app)

	#wait for input before quiting
	raw_input('any key to exit')
	main.framework.close()
	main.speach_routine.stop()


if __name__ == "__main__":
	main()






