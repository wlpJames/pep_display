#|||||||||||||||||||||||||||||||||||||||||||||||||||||||#
#                                                       #
#         A SCRIPT TO TEST THE COMS SERVICE FROM        #
#               A SEPARATE PYTHON TERMINAL              #
#                                                       #
#|||||||||||||||||||||||||||||||||||||||||||||||||||||||#


import qi
import sys
import json


app = qi.Application()
app.start()
session = app.session
coms = session.service("tablet_coms")

#callback for data return
def pepper_output_callback(data):

  print data

#regester callback
coms.pepper_to_tablet.connect(pepper_output_callback)

#send request
data = {'a':1.25, 'b':2, 'c':'this is a message'}
message = {'request_type' : 'GET', 'route' : 'index', 'data' : data}
coms.send_request_to_pepper(json.dumps(message))
